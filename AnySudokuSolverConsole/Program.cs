﻿using AnySudokuSolverLibrary;

namespace AnySudokuSolverConsole
{
    class Program
    {
        static void Main()
        {
            string easy = "4.....8.5.3..........7......2.....6.....8.4......1.......6.3.7.5..2.....1.4......";
            string hardest = ".....6....59.....82....8....45........3........6..3.54...325..6..................";   // very hard, solving take too much time, must be repaired
            string hard = ".2..65...7.....8..9..1....26....1.84.9.2.3.1.34.8....55....4..6..9.....8...68..4.";

            AnySudokuSolver.SolveAll(new string[] { hard, easy });
        }
    }
}
