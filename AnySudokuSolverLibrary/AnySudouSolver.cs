﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AnySudokuSolverLibrary
{
    public static class AnySudokuSolver
    {
        /// <summary>
        /// Constants used to navigate in sudoku game board.
        /// </summary>
        const string cols = "123456789";
        const string rows = "ABCDEFGHI";

        static string[] squares = Cross(rows, cols);
        static string[][] unitlist = CreateUnitList();
        static Dictionary<string, string[][]> units = CreateUnits();
        static Dictionary<string, string[]> peers = CreatePeers();


        /// <summary>
        /// Combine two strings.
        /// </summary>
        /// <param name="A">First string.</param>
        /// <param name="B">Second string.</param>
        /// <returns>Array of combined strings</returns>
        static string[] Cross(string A, string B)
        {
            string[] result = new string[A.Length * B.Length];

            for (int i = 0; i < A.Length; i++)
                for (int y = 0; y < B.Length; y++)
                    result[i * B.Length + y] = A[i].ToString() + B[y].ToString();

            return result;
        }
        /// <summary>
        /// Create list of units in sudoku game board.
        /// </summary>
        /// <returns>Array of units.</returns>
        static string[][] CreateUnitList()
        {
            List<string[]> unitlist = new List<string[]>();

            foreach (char c in cols)
                unitlist.Add(Cross(rows, c.ToString()));
            foreach (char r in rows)
                unitlist.Add(Cross(r.ToString(), cols));
            foreach (string s1 in new string[] { "ABC", "DEF", "GHI" })
                foreach (string s2 in new string[] { "123", "456", "789" })
                    unitlist.Add(Cross(s1, s2));

            return unitlist.ToArray();
        }
        /// <summary>
        /// Associate each unit with his square.
        /// </summary>
        /// <returns>Dictionary, where keys are squares and values are units.</returns>
        static Dictionary<string, string[][]> CreateUnits()
        {
            Dictionary<string, string[][]> units = new Dictionary<string, string[][]>();
            List<string[]> temp = new List<string[]>();

            foreach (string s in squares)
            {
                temp.Clear();

                foreach (string[] u in unitlist)
                {
                    if (((IList<string>)u).Contains(s))
                    {
                        temp.Add(u);
                    }
                }

                units.Add(s, temp.ToArray());
            }

            return units;
        }
        /// <summary>
        /// Associate each sqare with each unit in which is contained.
        /// </summary>
        /// <returns>Dictionary, where keys are squares and values are peers.</returns>
        static Dictionary<string, string[]> CreatePeers()
        {
            Dictionary<string, string[]> peers = new Dictionary<string, string[]>();
            List<string> temp = new List<string>();

            foreach (string s in squares)
            {
                temp.Clear();

                foreach (string[] u in units[s])
                    foreach (string t in u)
                        if (!temp.Contains(t) && t != s)
                            temp.Add(t);

                peers.Add(s, temp.ToArray());
            }

            return peers;
        }
        /// <summary>
        /// Parse sudoku game board and set possible values for each square.
        /// </summary>
        /// <param name="grid">Sudoku game board.</param>
        /// <returns>Dictionary, where keys are sqares and values are possible numbers to fill in.</returns>
        static Dictionary<string, string> ParseGrid(string grid)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();

            foreach (string s in squares)
                values.Add(s, cols);

            foreach (KeyValuePair<string, string> p in GridValues(grid))
            {
                if (cols.Contains(p.Value.ToString()))
                {
                    Assign(values, p.Key, p.Value);
                }
            }

            return values;
        }
        /// <summary>
        /// Associate grid values with squares.
        /// </summary>
        /// <param name="grid">Sudoku game board.</param>
        /// <returns>Dictionary, where keys are squares and values are values filled in sudoku game board.</returns>
        static Dictionary<string, string> GridValues(string grid)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();

            if (grid.Length != squares.Length)
                throw new Exception("Grid values count doesn't equals to squares count");

            for (int i = 0; i < grid.Length; i++)
            {
                values.Add(squares[i], grid[i].ToString());
            }

            return values;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <param name="s"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        static Dictionary<string, string> Assign(Dictionary<string, string> values, string s, string d)
        {
            string otherValues = values[s].Replace(d,"");

            foreach (char d2 in otherValues)
            {
                if (Eliminate(values, s, d2.ToString()) == null)
                    return null;
            }

            return values;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <param name="s"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        static Dictionary<string, string> Eliminate(Dictionary<string, string> values, string s, string d)
        {
            if (!values[s].Contains(d))
                return values;

            if(d.Length != 0)
                values[s] = values[s].Replace(d, "");

            if (values[s].Length == 0)
                return null;
            else if (values[s].Length == 1)
                foreach (string s2 in peers[s])
                    if (Eliminate(values, s2, values[s]) == null)
                        return null;

            List<string> dplaces = new List<string>();

            foreach (string[] u in units[s])
            {
                dplaces.Clear();

                foreach (string x in u)
                    if (values[x].Contains(d)) dplaces.Add(x);

                if (dplaces.Count == 0) return null;

                if (dplaces.Count == 1)
                {
                    if(Assign(values, dplaces[0], d) == null)
                        return null;
                }
            }

            return values;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        public static void Display(Dictionary<string, string> values)
        {
            int width = 0;
            foreach (string s in squares)
            {
                if (width < values[s].Length)
                    width = values[s].Length;
            }
            width++;

            string line = "";

            for (int x = 0; x < 3; x++)
            {
                for (int i = 0; i < 3 * width; i++) line += '-';
                if(x!=2) line += '+';
            }

            string result = "";
            int index = 0;

            foreach (string s in squares)
            {
                if (index == 27 || index == 54)
                    result += "\n" + line;

                if (index != 0 && index % 9 == 0)
                    result += "\n";

                if (index%9 !=0 && index % 3 * width == 0)
                {
                    result += "|";
                }

                string test = values[s].Center(width);
                result += values[s].Center(width);
                index++;
            }

            Console.Write(result + "\n\n");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        static Dictionary<string, string> Search(Dictionary<string, string> values)
        {
            var res = from s in squares
                      where values[s].Length > 1
                      select s;

            if (res == null)
                return values;

            var val = from s in squares
                      where values[s].Length > 1
                      select new KeyValuePair<int, string>(values[s].Length, s);

            if (values != null && val.Count() != 0)
            {
                int n = val.Min<KeyValuePair<int, string>>(k => k.Key);
                string str = val.First<KeyValuePair<int, string>>(x => x.Key == n).Value;

                List<Dictionary<string, string>> test = new List<Dictionary<string, string>>();


                foreach (char d in values[str])
                {
                    Dictionary<string, string> temp = new Dictionary<string, string>(values);

                    test.Add(Search(Assign(temp, str, d.ToString())));
                }

                return Some(test);
            }
            return values;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        static Dictionary<string, string> Some(List<Dictionary<string, string>> seq)
        {
            foreach (Dictionary<string, string> p in seq)
            {
                if (p != null) return p;
            }
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        public static Dictionary<string,string> Solve(string grid)
        {
            return Search(ParseGrid(grid));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="grids"></param>
        public static void SolveAll(string[] grids)
        {
            if (grids.Length == 0)
                return;

            DateTime startTime = DateTime.Now;

            foreach (string grid in grids)
            {
                Display(Solve(grid));
            }

            TimeSpan endTime = DateTime.Now - startTime;

            Console.WriteLine("Total time: " + endTime.TotalSeconds.ToString() + " sec\n");
        }


        static void Main()
        {
            string easy    = "4.....8.5.3..........7......2.....6.....8.4......1.......6.3.7.5..2.....1.4......";
            string hardest = ".....6....59.....82....8....45........3........6..3.54...325..6..................";   // very hard, solving take too much time, must be repaired
            string hard    = ".2..65...7.....8..9..1....26....1.84.9.2.3.1.34.8....55....4..6..9.....8...68..4.";

            SolveAll(new string[]{hard, easy});
            Display(ParseGrid(hard));
        }
    }
}
