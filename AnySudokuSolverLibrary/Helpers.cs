﻿using System;

namespace AnySudokuSolverLibrary
{
    public static class Helpers
    {
        public static string Center(this string s, int size)
        {
            string res = new String(new char[size]);

            if (s.Length >= size)
                return s;

            int startIndex = (size - s.Length) / 2;

            res = res.Insert(startIndex, s);
            res = res.Remove(startIndex + s.Length, s.Length);

            return res;
        }
    }
}
